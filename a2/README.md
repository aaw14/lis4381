

# LIS 4381

## Alexis Wood 

### Assignment 2# Requirements:

*Four Parts:*

1. Create running application's first user interface 
2. Create running application's second user interface 
3. Questions 
4. Push to Bitbucket Repo 

#### README.md file should include the following items:

* Screenshot of running application's first user interface 
* Screenshot of running application's second user interface 

#### Assignment Screenshots:

*Screenshot of first interface running*:

![AMPPS Installation Screenshot](img/first.png)

*Screenshot of second interface running*:

![JDK Installation Screenshot](img/second.png)
