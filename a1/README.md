

# LIS 4381

## Alexis Wood 

### Assignment 1# Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations 
3. Questions 
4. Bitbucket repo links 

#### README.md file should include the following items:

* Screenshot of hwapp application running
* Screenshot of aspnet:creapp application running My .NET Core Installation
* git commands with short descriptions 



>
> #### Git commands w/short descriptions:

1. git init - initializes a git repository 
2. git status - shows status of files in the index vs working directory 
3. git add - adds files changes in your working directory to your index 
4. git commit - takes all commands written in the index and creates a new commit object pointing to it and sets new branch pointing to it 
5. git push - pushes all modified local objects to the remote repository and merges with local 
6. git pull - fetches fies from remote repository and merges with local 
7. git clone - makes a git repository copy from a remote source 

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/amppss.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/androidscreen.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/aaw14/bitbucketstationlocation/ "Bitbucket Station Location")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/aaw14/myteamquotes/ "My Team Quotes Tutorial")
