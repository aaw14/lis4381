
>

# LIS43868

## Alexis Wood

### A4# Requirements:

*Four Requirements:*

1. Create My Online Portfolio 
2. Tested jQuery Validation and used HTML5 to properly limit the number of characters per control 
3. Create Basic Client Side Validation
4. Chapter Questions

#### README.md file should include the following items:


* [index](http://localhost:8080/lis4381/a4/index.jsp)



> 

>


#### Assignment Screenshots:

*Screenshot of My Online Portfolio http://localhost:8080/lis4381/index.jsp*:
*Screenshot of Basic Client Side Validation*

![Online Portfolio Screenshot](img/p1.png)
![Validation Screenshot](img/p1a.png)
![Portfolio Screenshot](img/port.png)
