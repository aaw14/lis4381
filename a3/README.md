
>

# LIS4381

## Alexis Wood

### Assignment 3# Requirements:

*Three Requirements:*

1. Screeenshots of Ticket Calculator
2. Screenshot of ERD 
3. Links to a3.mwb and a3.sql


#### README.md file should include the following items:

* Screenshot of running application's first user interface 
* Screenshot of running application's second user interface 
* [a3.sql](docs/a3.sql)
* [a3.mwb](docs/a3.mwb)


#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/a3.png)

*Screenshot of first interface running*:

![AMPPS Installation Screenshot](img/first.png)

*Screenshot of second interface running*:

![JDK Installation Screenshot](img/second.png)




