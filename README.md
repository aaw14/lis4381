> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Advanced Web Application Development 

## Alexis Wood 

### Course Work links 



*Screenshot of AMPPS running http://localhost*:

[A1 README.md](a1/README.md "My A1 README.md file")

* Install .NET Core 
* Create hwapp application 
* Create aspnetcoreapp application 
* Provide screenshots of installations 
* Create Bitbucket repo
* Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
* Provide git command descriptions 

[A2 README.md](a2/README.md "My A2 README.md file")

* Familiarize myself with Andriod Studio
* Create running application's first user interface 
* Create running application's second user interface 

[A3 README.md](a3/README.md "My A3 README.md file")

* Familiarize myself with Andriod Studio
* Create running application's first user interface 
* Create running application's second user interface 
* Create Pet Store ERD 

[P1 README.md](p1/README.md "My P1 README.md file")

* Create running application's first user interface 
* Create running application's second user interface 
* Chapter questions 

[A4 README.md](a4/README.md "My A4 README.md file")

* Create running web application 
* Create running application with form validation
* Create Online Portfolio

[A5 README.md](a5/README.md "My A5 README.md file")

* Create running web application 
* Create running application with server side validation
* Create Online Portfolio

[P2 README.md](p2/README.md "My P2 README.md file")

* Create running web application 
* Create functionality to edit and delete
* Create Online Portfolio